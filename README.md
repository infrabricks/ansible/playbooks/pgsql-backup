# PG operations

Ansible playbook to launch operations on postgreSQL bases.

## Requirements

* You need this role to use this playbook : [PGSQL backup](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-backup)
* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name     | Variable description        | Type    | Default | Required
---               | ---                         | ---     | ---     | ---
pgsql_op          | pgsql op (backup / restore) | string  | no      | yes
pgsql_bckdir      | pgsql backup dir            | string  | yes     | yes
pgsql_dbname      | pgsql db name               | string  | no      | yes

## Playbook Example

An example of playbook

```
- name: Pgsql backup
  hosts: all
  vars:
    pgsql_dbname: mydb
    pgsql_op: backup
  tasks:
    - name: Include PGSQL backup role
      ansible.builtin.include_role:
        name: pgsql_backup
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
